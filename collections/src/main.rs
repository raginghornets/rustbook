use std::collections::HashMap;

fn main() {
    let v = vec![6, 4, 2, 1, 6, 3];
    println!("mean: {}", mean(&v));
    println!("medium: {}", medium(&v));
    println!("mode: {}", mode(&v));

    let word = "potato";
    println!("pig_latin of '{}': {}", word, pig_latin(String::from(word)));

    let mut company = company::new();
    company.start_interface();
}

fn mean(v: &Vec<i32>) -> i32 {
    if v.is_empty() {
        return 0;
    }
    v.iter().sum::<i32>() / v.len() as i32
}

fn medium(v: &Vec<i32>) -> i32 {
    let mut clone = v.clone();
    clone.sort();
    *clone.get(clone.len() / 2).unwrap_or(&0)
}

fn mode(v: &Vec<i32>) -> i32 {
    let mut map = HashMap::new();
    for n in v {
        let count = map.entry(n).or_insert(0);
        *count += 1;
    }
    let mut max = (&0, 0);
    for (key, value) in map {
        if value > max.1 {
            max = (key, value);
        }
    }
    *max.0
}

fn pig_latin(word: String) -> String {
    if word.is_empty() {
        return word;
    }
    match &word.trim_start()[..1] {
        "a" | "e" | "i" | "o" | "u" => format!("{}-hay", &word.trim()),
        _ => format!("{}-{}ay", &word.trim()[1..], &word.trim()[..1]),
    }
}

mod company {
    use std::{collections::HashMap, io::Write};

    #[derive(Debug, Hash)]
    enum Department {
        Engineering,
        Sales,
        Hiring,
    }

    impl PartialEq for Department {
        fn eq(&self, other: &Self) -> bool {
            std::mem::discriminant(self) == std::mem::discriminant(other)
        }
    }

    impl Eq for Department {}

    #[derive(Debug)]
    struct Employee {
        name: String,
    }

    pub struct Company {
        employees: HashMap<Department, Vec<Employee>>,
    }

    pub fn new() -> Company {
        Company {
            employees: HashMap::new(),
        }
    }

    impl Company {
        fn add_employee(&mut self, department: Department, employee: Employee) {
            self.employees
                .entry(department)
                .or_insert(Vec::new())
                .push(employee);
        }

        pub fn start_interface(&mut self) {
            println!("Starting interface.");

            loop {
                print!("> ");
                std::io::stdout().flush().unwrap();

                let mut query = String::new();

                std::io::stdin()
                    .read_line(&mut query)
                    .expect("Failed to read line");

                let query: Vec<&str> = query.trim().split(' ').collect();

                match query.first() {
                    Some(&"add") => {
                        if query.len() == 4 && *query.get(2).unwrap() == "to" {
                            let employee = Employee {
                                name: String::from(*query.get(1).unwrap()),
                            };
                            let department = match *query.get(3).unwrap() {
                                "engineering" => Department::Engineering,
                                "sales" => Department::Sales,
                                "hiring" => Department::Hiring,
                                _ => {
                                    println!(
                                        "Department '{}' does not exist.",
                                        *query.get(3).unwrap()
                                    );
                                    continue;
                                }
                            };
                            println!(
                                "Added {} to the {:?} department.",
                                &employee.name, &department
                            );
                            &self.add_employee(department, employee);
                        } else {
                            println!("Usage: add [employee] to [department]");
                            continue;
                        }
                    }
                    Some(&"show") => {
                        println!("List of employees:");
                        println!("{:?}", &self.employees);
                    }
                    Some(&"quit") => break,
                    Some(&"help") => {
                        println!("Available commands:");
                        println!("\tadd - Add an employee to a department");
                        println!("\tshow - List all employees in the company");
                        println!("\tquit - Quit the session");
                    }
                    Some(&"") => continue,
                    _ => println!("Invalid query. Type 'help' to list the available commands."),
                }
            }
        }
    }
}
