use cons::List::{Cons,Nil};

fn main() {
    let list = Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil))))));
    println!("{}", list);
    println!("{}", list.tail());
    println!("{}", list.tail().tail());
    println!("{}", list.tail().tail().tail());
}
